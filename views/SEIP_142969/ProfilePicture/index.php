
<head>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->






</head>
<?php
require_once ("../../../vendor/autoload.php");
use App\ProfilePicture\ProfilePicture;
use App\Message\Message;
$objProfilePicture=new ProfilePicture();

$allData=$objProfilePicture->index("obj");

$serial=1;
echo "<table border='2px'>";
echo "<th>Serial</th><th>id</th><th>Name</th><th>Image</th><th>Action</th>";
foreach($allData as $oneData){

    echo "<tr>";

    echo "<td> $serial</td>";
    echo "<td> $oneData->id</td>";
    echo "<td> $oneData->name</td>";
    echo "<td> $oneData->image</td>";
    echo "<td> <a href='view.php?id=$oneData->id'><button class='btn-info'>View</button></a>
     <a href='edit.php?id=$oneData->id'><button class='btn-default'>Edit</button></a>
     <a href='trash.php?id=$oneData->id'><button class='btn-default'>Trash</button></a>
     <a href='delete.php?id=$oneData->id'><button class='btn-danger'>Delete</button></a></td>";


    echo "</tr>";
    $serial++;

   // echo $serial++.','.$oneData->id.','.$oneData->booktitle."<br>";
}//end of foreach loop

echo "</table>";
