<?php
namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $image;
    public function __construct()
    {
        parent::__construct();
    }


    public function setData($postVariableData=Null)
    {
        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists('name',$postVariableData))
        {
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists('image',$postVariableData))
        {
            $this->image=$postVariableData['image']['name'];
        }

    }
    public function store()
    {
        $path='C:\xampp\htdocs\Nazmun_nahar_SEIP_ID_142969_view_edit_delete_trash_LAB_EXAM_08\Resource\Upload/';
        $uploadedFile = $path.basename($this->image);
        move_uploaded_file($_FILES['image']['tmp_name'], $uploadedFile);
        $arrData =array($this->name,$this->image);

        $sql="INSERT INTO profile_picture(name,image) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::setMessage("Success!Data has been inserted successfully");
        else
            Message::setMessage("Failed!Data has been inserted successfully");

        Utility::redirect('create.php');
    }



    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query("SELECT * from profile_picture WHERE is_deleted='No'");

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from profile_picture WHERE id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arroneData  = $STH->fetch();
        return $arroneData;


    }// end of view();

    public function update(){

         $path='C:\xampp\htdocs\Nazmun_nahar_SEIP_ID_142969_view_edit_delete_trash_LAB_EXAM_08\Resource\Upload/';
        if(!empty($_FILES['image']['name'])) {
            $uploadedFile = $path . basename($this->image);
            move_uploaded_file($_FILES['image']['tmp_name'], $uploadedFile);
            $arrData = array($this->name, $this->image);
            $sql = "UPDATE profile_picture SET name = ?,image = ? WHERE id =" . $this->id;
        }
             else{
                $arrData=array($this->name);
                $sql="UPDATE profile_picture SET name = ? WHERE id =".$this->id;
            }

        $STH = $this->DBH->prepare($sql);
        $STH->execute( $arrData);

        Utility::redirect('index.php');



    }//end of update method


    public function delete(){
        $arrData=array($this->name,$this->image);
        $sql="DELETE from profile_picture  WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Deleted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Deleted Successfully :( ");

        Utility::redirect('index.php');



    }//end of delete method


    public function trash(){

        $sql = "Update profile_picture SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash(

    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from profile_picture where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update profile_picture SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();
}