<?php

namespace App\Hobbies;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Hobbies extends DB{
    public $id;
    public $name;
    public $hobbies;




    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];
        }

        if (array_key_exists('hobbies', $postVariableData)) {
           $arr = $postVariableData['hobbies'];
            $this->hobbies=implode(',',$arr);
            //echo $this->hobbies;Die();
        }


    }

    public function store()
    {
        $arrData=array($this->name,$this->hobbies);
        $sql = "Insert INTO hobbies(name,hobbies) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Insurted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Insurted Successfully :( ");

        Utility::redirect('create.php');


    }//end of store mathod

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query("SELECT * from hobbies WHERE is_deleted='No'");

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from hobbies WHERE id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arroneData  = $STH->fetch();
        return $arroneData;


    }// end of view();

    public function update(){

        $arrData=array($this->name,$this->hobbies);
        $sql="UPDATE hobbies SET name = ?,hobbies = ? WHERE id =".$this->id;
        //var_dump($sql);die;

        $STH = $this->DBH->prepare($sql);
        $STH->execute( $arrData);

        Utility::redirect('index.php');



    }//end of update method



    public function delete(){
        $arrData=array($this->name,$this->hobbies);
        $sql="DELETE from hobbies  WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Deleted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Deleted Successfully :( ");

        Utility::redirect('index.php');



    }//end of delete method


    public function trash(){

        $sql = "Update hobbies SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()

    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from hobbies where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update hobbies SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();
}